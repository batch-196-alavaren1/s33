
// express package was imported as express.
const express = require("express");

// invoke express package to create a server/api and saved it in a variable which we can refer to later to create routes.
const app = express();


// express.json() is a method from express that allow us to handle the stream of data from our client and receive the data and automatically parse the incoming JSON from the request.

// app.use() is a method used to run another function or method for our expressjs api.
// It is used to run middlewares (functions that add features to our application)
app.use(express.json())

// variable for port assignment
const port = 4000;


/*Mock collection for Courses*/
let courses = [

		{

			name: "Python 101",
			description: "Learn Python",
			price: 25000

		},
		{

			name: "React 101",
			description: "Learn React",
			price: 35000

		},
		{

			name: "ExpressJS 101",
			description: "Learn ExpressJS",
			price: 28000

		}

];

let users = [

	{
		username: "marybell_knight",
		password: "merrymarybell"
	},
	{
		username: "janedoePriest",
		password: "jobPriest100"
	},
	{
		username: "kimTofu",
		password: "dubuTofu"
	}


];

// used the listen() method of express to assign a port to our server and send a message

//creating a route in Express:
// access express to have access to its route methods.
/*
	app.method('/endpoint',(request,response)=>{
		
		//send() is a method similar to end() that it sends the data/message and ends the response
		//It also automatically creates and adds the headers

		respond.send()
	})
*/

app.get('/',(req,res)=>{

	res.send("Hello from our first ExpressJS route!");

})

app.post('/',(req,res)=>{
	res.send("Hello from our first ExpressJS Post Method Route!")
})

/*
	Mini Activity
*/
app.put('/',(req,res)=>{
	res.send("Hello from a put method route!")
})

app.delete('/',(req,res)=>{
	res.send("Hello from a delete method route!")
})




app.get('/courses',(req,res)=>{

	res.send(courses);
})

// Create a route to be able to add a new course from an input from a request.
app.post('/courses',(req,res)=>{


	// with express.json() the data stream has been captured, the data input has been parsed into a JS Object.

	// Note: Everytime you need to access or use the request body, log it in the console first.
	// console.log(req.body)//object
	// request/req.body contains the body of the request or the input passed.


	// add the input as req.body into our courses array
	courses.push(req.body);

	// console.log(courses);

	// send the 
	res.send(courses);



})
/*
	Activity
*/
app.get('/users',(req,res)=>{

	res.send(users);
})

app.post('/users',(req,res)=>{

	// console.log(req.body)check if you can recieve the body of the request
	/* {
	
		username: <value>,
		password: <value>

		}
	*/
	users.push(req.body);
	// console.log(users);
	res.send(users);

})

// delete the last item 


// update the course route
// app.put('/course',(req,res)=>{
// 	console.log(req,body);


// 	let index = parseInt(req.body.index);
// 	course[index].price = req.body.price;
// 	res.send(courses[index])
// });


app.listen(port,() => console.log(`Express API running at port 4000`))



